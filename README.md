# mutatis - toy project for on-the-fly API migrations
The idea: Proceed with backwards-incompatible API changes to
data schemas and define programmatically how to transform data into
the new form. Thereafter, `mutatis` handles clients which can update
in their own pace. No need to run multiple API versions at the same
time.

This all works as an extension to Pydantic that validates and transforms
the data into the latest version.

# Examples
Say you have an endpoint that accepts desserts and gives you the amount
of calories in return:

#### Request
```python
from pydantic import BaseModel
class Cake(BaseModel):
    type: str
    ingredients: str
```
```json
{
  "type": "Sacher Torte",
  "ingredients": "sugar, chocolate, cream"
}
```
#### Response
```json
{
  "calories": "a lot"
}
```

But then you decide, quite reasonably, that the `ingredients` field should
rather be an array, so you proceed with a backwards-incompatible array:
```python
# api.py
from typing import List
from pydantic import BaseModel
class Cake(BaseModel):
    type: str
    ingredients: List[str]
```

Now you have several options with regards your previous API version:
1. Disregard all the clients that were using your API.
   - an enviable solution that not many software engineers have, though
     if `len(clients) == 0` a very viable one.
2. Wait until all clients that were using your API update their software
   and release everything at precisely the same time.
   - dreamy.
3. Set the type of `ingredients` to `Union[List[str], str]` and add an
   `if isinstance(data.ingredients, str)` to you API method handler.
   - OK if you have one small change like this. Starts not to be OK
     once there are changes every Sprint, then it's the technical debt
     accumulates and bam, _spaghetti_.
4. Subclass from `MutatisModel`, create a transformation operation and let
   `mutatis` handle this.
   - `MutatisModel` adds a (configurable) version field used
     to figure out which transformations are necessary on request data.

In practice, it could look something like this:

```python
# api.py
from typing import List
from mutatis import MutatisModel
class Cake(MutatisModel):
    type: str
    ingredients: List[str]
```

```python
# migrations.py
class Cake:
    @staticmethod
    def version_1(old_cake: dict) -> dict:  # todo transformations will be correctly typed
        new_cake = old_cake # no need to deepcopy here
        new_cake['ingredients'] = [
            ingr.trim() for ingr in old_cake['ingredients'].split(',')
        ]
        return new_cake

    @staticmethod
    def version_2(old_cake: dict) -> dict:
        # next transformation
        pass
```

# Advantages:
1. Keeps the backwards-compatibility handling outside your
API since transformations are kept typically in a different module.
2. APIs are forever - but it doesn't need to be at the cost of readability.
3. (todo) Validates the data across all versions.
4. Compared to using a `Union[List[str], str]`, there is no need to
   handle different types in the API code. This means less branching,
   less complexity to reason about.
5. Migrations can be specifically tested without bringing in other
   API or even business logic. It is _independent_.


# TODOs
- automatically run a transform on the returned data
- option to cache the model_id and revision_id of loaded migration classes
- find out how to better enable configuration of revision_id other than using FieldInfo `desciprion`
- optional revision id works with default
- store previous schemas and do type checked migrations
- testing of migrations using hypothesis + collections of user-defined data
- allow to have the migration id defined by MutatisModel

# Contributions
Do you like this idea? I am just starting, but would be great to hear some
feedback already. Let me know in a new Issue.
