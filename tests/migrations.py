class BasicModel:
    @staticmethod
    def v0(old: dict) -> dict:
        print("base")
        return old

    @staticmethod
    def v1(old: dict) -> dict:
        print("migration to v1")
        new = old
        for k, v in new.items():
            if isinstance(v, int):
                new[k] = 1
        return new

    @staticmethod
    def v2(old: dict) -> dict:
        print("migration to v2")
        new = old
        for k, v in new.items():
            if isinstance(v, str):
                new[k] = "v2"
        return new


class DifferentRevisionId:
    pass


class WasStringBefore:
    @staticmethod
    def release_1(old: dict) -> dict:
        return old

    @staticmethod
    def release_1_1(old: dict) -> dict:
        seq: str = old.get("sequence")
        old["sequence"] = seq.split(",")
        return old
