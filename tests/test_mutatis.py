from typing import Dict, List, Sequence, Tuple

import pytest
from pydantic import ValidationError, validator
from pydantic.errors import IntegerError, MissingError

from mutatis import MutatisModel, RevisionIdField, configure
from tests import migrations


def test_check_missing_configuration():
    """
    order important for this test, needs to run before configure()
    """
    with pytest.raises(ValidationError) as e:
        WasStringBefore(sequence="")
    assert isinstance(e.value.raw_errors[0].exc, ValueError)
    assert "Configure module" in str(e.value.raw_errors[0].exc)


class BasicModel(MutatisModel):
    revision_id: str = RevisionIdField(default="latest", description="Revision ID")
    a: int
    b: str
    c: List[str]
    d: Tuple[str]
    e: Dict[str, int]

    @validator("e")
    def empty(cls, v):
        if len(v) > 0:
            raise ValueError("e has to be empty")
        return v


def test_validation_runs():
    configure(migrations)
    m = BasicModel(a=0, b="hi", c=[], d=("",), e={}, revision_id="v0")
    assert m.a == 1
    assert m.b == "v2"
    m = BasicModel(a=0, b="hi", c=[], d=("",), e={}, revision_id="v1")
    assert m.a == 0  # migration to v1 did not run
    assert m.b == "v2"

    with pytest.raises(ValidationError) as e:
        BasicModel(a="bad", b="hi", c=[], d=("",), e={"a": 1}, revision_id="v1")
    assert isinstance(e.value.raw_errors[0].exc, IntegerError)
    assert isinstance(e.value.raw_errors[1].exc, ValueError)


class DifferentRevisionId(MutatisModel):
    revision_id_different: str = RevisionIdField()
    a: int


def test_different_revision_id():
    configure(migrations)
    d = DifferentRevisionId(a=2, revision_id_different="latest")
    assert d.a == 2
    with pytest.raises(ValidationError) as e:
        DifferentRevisionId(a=1)
    assert isinstance(e.value.raw_errors[0].exc, MissingError)  # revision id is missing


class WasStringBefore(MutatisModel):
    revision_id: str = RevisionIdField()
    sequence: Sequence[int]


def test_change_to_new_type():
    configure(migrations)
    w = WasStringBefore(sequence="1,1,2,3,5,8,13", revision_id="base")
    assert len(w.sequence) == 7
    assert sum(w.sequence) == 33


class MissingId(MutatisModel):
    pass


def test_model_with_no_revision_id():
    with pytest.raises(ValidationError) as e:
        MissingId()
    assert isinstance(e.value.raw_errors[0].exc, ValueError)  # revision id is missing
    assert "Revision ID not found" in str(e.value.raw_errors[0].exc)


class MissingSchema(MutatisModel):
    revision_id: str = RevisionIdField()
    pass


def test_model_with_no_migrations_schema():
    configure(migrations)
    with pytest.raises(ValidationError) as e:
        MissingSchema()
    assert isinstance(e.value.raw_errors[0].exc, ValueError)  # revision id is missing
    assert "migrations.py" in str(e.value.raw_errors[0].exc)
