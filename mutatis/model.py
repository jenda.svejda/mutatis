import inspect

from pydantic import BaseModel, Field, root_validator

REVISION_ID = "Revision ID used by mutatis"

__migrations_module = None


def configure(migrations):
    global __migrations_module
    __migrations_module = migrations


class RevisionId(BaseModel):
    i: int


def RevisionIdField(**kwargs):
    """
    The type must implement str() which returns an ID ~ [a-zA-Z][a-zA-Z0-9_]*.
    IDs have to be ordered.
    """
    kwargs.pop("description", None)
    return Field(description=REVISION_ID, **kwargs)


class MutatisModel(BaseModel):
    @root_validator(pre=True)
    def __migrate(cls, values):
        print(f"Got these:   {values}")
        model = cls.__find_model()
        revision = cls.__find_revision(values)
        migrations = vars(cls.__migrations())
        model_revisions = migrations.get(model, None)
        return cls.__transform(model_revisions, revision, values)

    @classmethod
    def __transform(cls, model_revisions, revision, values):
        if model_revisions is None:
            raise ValueError(
                f"No revision schema for {cls.__name__} in {cls.__migrations()}"
            )
        ops = (
            n
            for n, op in vars(model_revisions).items()
            if inspect.ismethoddescriptor(op) and n > revision
        )
        for op in ops:
            revision_op = getattr(model_revisions, op, None)
            if callable(revision_op):
                values = revision_op(values)
                print(f"Migrated to: {values}")
        return values

    @classmethod
    def __find_model(cls):
        return cls.__name__

    @classmethod
    def __find_revision(cls, values):
        return values.get(cls.__get_revision_attribute(), "")

    @classmethod
    def __get_revision_attribute(cls):
        for k, f in cls.__fields__.items():
            if f.field_info.description is REVISION_ID:
                return k
        raise ValueError("Revision ID not found")

    @staticmethod
    def __migrations():
        migrations = globals()["__migrations_module"]
        if not migrations:
            raise ValueError("Configure module that contains migrations")
        return migrations
